package SampleHttp

import (
	"encoding/json"
	"errors"
	"fmt"
	"log"
	"net"
	"strconv"
	"strings"
	"sync"
	"time"
)

const (
	httpVersion = "HTTP/1.1"
	CRLF = "\r\n" // 定义行结束
	CRLF2 = "\r\n\r\n" // 定义首部报文结束符
)

var (
	route httpRoute
	statusCodeMsg map[uint]string
	SetResponseHeaderOnce sync.Once
)


// 请求报文
type RequestInfo struct {
	Method string
	Url string
	HttpVersion string

	RequestHeader map[string]string
	RequestData map[string]string //接收请求参数
	Body []byte

	RemoteIP string
}

// 响应报文
type ResponseInfo struct {
	HttpVersion string
	StatusCode uint
	StatusMsg string

	responseHeader map[string]string
	Body []byte
}

// GET路由
type httpRouteGet struct {
	lock sync.RWMutex
	routeGetMap map[string]func(info *HttpInfo)
}


// Post路由
type httpRoutePost struct {
	lock sync.RWMutex
	routePostMap map[string]func(info *HttpInfo)
}

// 路由
type httpRoute struct {
	get httpRouteGet
	post httpRoutePost
}

type HttpInfo struct {
	RequestInfo
	ResponseInfo
}

func init() {
	route.get.routeGetMap = make(map[string]func(info *HttpInfo),0)
	route.post.routePostMap = make(map[string]func(info *HttpInfo),0)

	statusCodeMsg = make(map[uint]string,0)
	statusCodeMsg[200] = "OK"
	statusCodeMsg[301] = "Moved Permanently"
	statusCodeMsg[400] = "Bad Request"
	statusCodeMsg[404] = "Not Found"
	statusCodeMsg[500] = "Internal Server Error"
	statusCodeMsg[504] = "Gatewat Time-out"
}

func StartServer(addr string) {
	listen , err := net.Listen("tcp",addr)
	if err != nil {
		panic(err)
	}

	for {
		conn , err := listen.Accept()
		if err != nil {
			log.Fatalln("net accept error " , err)
		}
		var info HttpInfo
		go info.worker(conn)
	}
}

// 记录路由
func Route(methods string,url string,funcName func(info *HttpInfo)) {
	methods = strings.ToUpper(methods)

	switch methods {
	case "POST":
		func() {
			route.post.lock.Lock()
			defer route.post.lock.Unlock()

			route.post.routePostMap[url] = funcName
		}()
	case "GET":
		func() {
			route.get.lock.Lock()
			defer route.get.lock.Unlock()

			route.get.routeGetMap[url] = funcName
		}()

	default:
		log.Println("未支持该协议")
	}
}

// 开启服务
func (info *HttpInfo)worker(conn net.Conn) {
	defer conn.Close()

	info.RequestInfo.RemoteIP = conn.RemoteAddr().String()

	// 获取http报文
	err := info.readReuestMessage(conn)
	if err != nil {
		fmt.Println("http read reuest msg error")
		return
	}

	var fun func(info *HttpInfo)
	var ok bool

	switch info.RequestInfo.Method {
	case "GET":
		func() {
			route.get.lock.RLock()
			defer route.get.lock.RUnlock()

			// 拆解url
			getData := strings.SplitN(info.RequestInfo.Url,"?",2)

			if 2 <= len(getData) {
				info.RequestInfo.RequestData = make(map[string]string,1)

				info.fromUrlencodeToRequestData(urlDecode(getData[1]))

				//for _,items := range strings.Split(urlDecode(getData[1]),"&") {
				//
				//	item := strings.Split(items,"=")
				//	if 1 < len(item) {
				//		key := item[0]
				//		val := item[1]
				//		info.RequestInfo.RequestData[key] = val
				//	}
				//}
				info.RequestInfo.Url = getData[0]
			}

			fun , ok = route.get.routeGetMap[info.RequestInfo.Url]
		}()
	case "POST":
		func() {
			route.post.lock.RLock()
			defer route.post.lock.RUnlock()

			// 判断请求头是否存在 Content-Type
			contentType,map_ok := info.RequestHeader["Content-Type"]
			if map_ok {
				if strings.HasPrefix(contentType,"application/x-www-form-urlencoded") {
					// application/x-www-form-urlencoded
					info.fromBodyGetPostData(1)

				} else if strings.HasPrefix(contentType,"application/json") {
					// application/json
					info.fromBodyGetPostData(2)
				}
			}

			fun , ok = route.post.routePostMap[info.RequestInfo.Url]
		}()
	default:
		return
	}
	if ok {
		startTime := time.Now()
		fun(info)
		log.Println(info.RequestInfo.Method,info.RequestInfo.Url,info.ResponseInfo.StatusCode,time.Since(startTime))
	}

	if ok {
		tcpWrite(conn,info.getResponseMessage())
	}
}

func (info *HttpInfo)fromBodyGetPostData(types uint8) {

	// 如果请求主体为空，则直接退出该方法
	if 0 == len(info.RequestInfo.Body) {
		return
	}

	// 申请map存储数据
	info.RequestInfo.RequestData = make(map[string]string,1)

	// 将url编码数据解析为正常数据
	datas := urlDecode(string(info.RequestInfo.Body))

	switch types {
	case 1:
		// application/x-www-form-urlencoded
		info.fromUrlencodeToRequestData(datas)

	case 2:
		// application/json
		info.fromJsonToRequestData(datas)
	}
}

// 从json中提取数据
func (info *HttpInfo)fromJsonToRequestData(datas string) {
	err := json.Unmarshal([]byte(datas),&info.RequestInfo.RequestData)
	if err != nil {
		log.Println("json Unmarshal error " , err)
	}
}

// 从urlencode提取数据至RequestInfo
func (info *HttpInfo)fromUrlencodeToRequestData(datas string) {
	for _,items := range strings.Split(datas,"&") {

		item := strings.Split(items,"=")
		if 1 < len(item) {
			key := item[0]
			val := item[1]
			info.RequestInfo.RequestData[key] = val
		}
	}
}

func tcpWrite(conn net.Conn,repBuf []byte) {
	// 将repBuf返回给客户端
	sendLen := 0
	for sendLen < len(repBuf) {
		n ,err := conn.Write(repBuf[sendLen:])
		if err != nil {
			fmt.Println(err)
			return
		}
		sendLen += n
	}
}

// 获取请求报文
func (info *HttpInfo)readReuestMessage(conn net.Conn) (error) {
	maxLen := 8182
	headBuf := make([]byte,maxLen) // 申请内存用于接收报文

	readSize := 0
	// 每次读取一个字节数据
	for i:=1;i<=maxLen;i++ {
		n,err := conn.Read(headBuf[readSize:i])
		if err != nil {
			fmt.Println("error: " , err)
			return err
		}
		readSize = readSize + n

		// 判断首部报文是否结束了
		if len(CRLF2) <= len(string(headBuf[:readSize])) {
			if string(headBuf[readSize-len(CRLF2):readSize]) == CRLF2 {
				break
			}
		}
	}

	// 判断首部报文是否超过允许接收长度
	if maxLen <= readSize {
		fmt.Println("超过允许接收的最大长度")
		return errors.New("超过允许接收的最大长度")
	}

	// 声明 httpHeaders
	info.RequestInfo.RequestHeader = make(map[string]string,0)

	// 将获取的首部数据打印出来
	for k,v := range strings.Split(string(headBuf[:readSize-len(CRLF)]),CRLF) {
		if v == "" {
			continue
		}

		if 0 == k {
			// 请求行
			header := strings.Split(v," ")
			info.RequestInfo.Method = header[0]
			info.RequestInfo.Url = header[1]
			info.RequestInfo.HttpVersion = header[2]
		} else {
			// 请求头
			requestLine := strings.Split(v,": ")
			info.RequestInfo.RequestHeader[requestLine[0]] = requestLine[1]
		}
	}

	// 获取报文主体数据
	bodyLen , ok := info.RequestInfo.RequestHeader["Content-Length"];if ok {
		bodyLenInt , err := strconv.Atoi(bodyLen)
		if err != nil {
			fmt.Println("Content-Length值转化失败")
			return err
		}

		// 从管道中取 Content-Length 长度的数据
		bodyBuf := make([]byte,bodyLenInt)
		recvLen := 0
		for recvLen < bodyLenInt {
			n , err := conn.Read(bodyBuf[recvLen:bodyLenInt])
			if err != nil {
				fmt.Println("read error " , err)
				return err
			}
			recvLen += n
		}
		info.RequestInfo.Body = bodyBuf[:]
	}
	return nil
}

// 状态码
func (info *HttpInfo)SetHttpStatusCode(code uint) {
	info.ResponseInfo.StatusCode = code

	codeMsg , ok := statusCodeMsg[info.ResponseInfo.StatusCode];if ok {
		info.ResponseInfo.StatusMsg = codeMsg
	}
}

// 数据
func (info *HttpInfo)Write(msg []byte) {
	info.ResponseInfo.Body = msg
}

func (info *HttpInfo)SetResponseHeader(key string,value string) {
	if nil == info.ResponseInfo.responseHeader {
		info.ResponseInfo.responseHeader = make(map[string]string,0)
	}
	info.ResponseInfo.responseHeader[key] = value
}

// 生成响应报文
func (info *HttpInfo)getResponseMessage()([]byte) {
	responseByte := make([]byte,0)
	// 构建状态行
	// 版本
	responseByte = append(responseByte, []byte(httpVersion+" ")...)

	// 状态码
	responseByte = append(responseByte, []byte(strconv.Itoa(int(info.ResponseInfo.StatusCode))+" ")...)

	// 短语
	responseByte = append(responseByte, []byte(info.ResponseInfo.StatusMsg+CRLF)...)

	// 首部信息
	for k,v := range info.ResponseInfo.responseHeader {
		responseByte = append(responseByte, []byte(k+": ")...)
		responseByte = append(responseByte, []byte(v+CRLF)...)
	}

	// 添加响应主体
	if 0 < len(info.ResponseInfo.Body) {
		// 新增 Content-Length
		//info.ResponseInfo.Body = append(info.ResponseInfo.Body, []byte(CRLF)...)
		responseByte = append(responseByte, []byte("Content-Length: "+strconv.Itoa(len(info.ResponseInfo.Body)))...)
	}

	responseByte = append(responseByte, []byte(CRLF2)...)
	responseByte = append(responseByte, info.ResponseInfo.Body...)

	//fmt.Println("即将发送至客户端数据: " , string(responseByte))
	return responseByte
}

// 16进制转10进制
func unhex(c byte) byte {
	switch {
	case '0' <= c && c <= '9':
		return c - '0'
	case 'a' <= c && c <= 'f':
		return c - 'a' + 10
	case 'A' <= c && c <= 'F':
		return c - 'A' + 10
	}
	return 0
}

// urlDecode
func urlDecode(s string) (string) {
	// 获取有多少个需要转义的
	n := 0
	for _,v := range s {
		if '%' == v {
			n = n + 2 // 我们编码需要三个字符，如: %E5，我们计算长度的时候，需要减去2个
		}
	}
	var t strings.Builder // 声明一个 strings.Builder
	t.Grow(len(s) - n) // 申请 len(s) -n 个容量

	for i := 0; i < len(s); i++ {
		switch s[i] {
		case '%': // 需要进行解密
			t.WriteByte(unhex(s[i+1])<<4 | unhex(s[i+2]))
			i += 2
		case '+': // +号，在url编码中为 空格
			t.WriteByte(' ')
		default:
			t.WriteByte(s[i])
		}
	}

	return t.String()
}