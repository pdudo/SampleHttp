# SampleHttp

#### 介绍
用于探寻http协议


#### 使用`samplehttp` 
```go
package main

import (
	"fmt"
	"gitee.com/pdudo/SampleHttp"
	"log"
)

func login(info *SampleHttp.HttpInfo) {
	username := info.RequestInfo.RequestData["username"]
	password := info.RequestInfo.RequestData["密码"]

	log.Println(info)

	toClientString := fmt.Sprintf("接收到POST请求参数,username: %s , 密码: %s",username,password)
	info.Write([]byte(toClientString))
}


func main() {

    SampleHttp.Route("post","/login",login)

	// 请请求 /site?名字=1232
    SampleHttp.Route("GET","/site", func(info *SampleHttp.HttpInfo) {
		info.SetHttpStatusCode(200)
		name := info.RequestInfo.RequestData["名字"]
		info.SetResponseHeader("Content-Type","text/html;charset=utf-8")

		log.Println(info)
		toClientString := fmt.Sprintf("路由已经收到参数: name: %s" ,name)
		info.Write([]byte(toClientString))
	})

	SampleHttp.Route("GET","/123", func(info *SampleHttp.HttpInfo) {
		info.SetHttpStatusCode(301)
		info.SetResponseHeader("Location","https://juejin.cn")
	})

	SampleHttp.StartServer("0.0.0.0:8082")
}
```
